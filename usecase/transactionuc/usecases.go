// Package registration represents the concrete implementation of TransactionUseCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package transactionuc

import (
	"errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/maxine/model"
	"gitlab.com/zenpagos/maxine/repository"
	"gitlab.com/zenpagos/maxine/usecase"
	"gitlab.com/zenpagos/tools"
)

type TransactionUseCase struct {
	log                   *logrus.Logger
	TransactionRepository repository.TransactionRepositoryInterface
	TransactionOperation  repository.TransactionOperationInterface
}

func NewTransactionUseCase(
	log *logrus.Logger,
	tr repository.TransactionRepositoryInterface,
	to repository.TransactionOperationInterface,
) usecase.TransactionUseCaseInterface {
	return TransactionUseCase{
		log:                   log,
		TransactionRepository: tr,
		TransactionOperation:  to,
	}
}

func (uc TransactionUseCase) CreateTransactionFromEntry(e *model.Entry) error {
	switch e.Source {
	case model.PaymentSource:
		return uc.createDepositTransaction(e)
	case model.CashOutSource:
		return uc.createWithdrawalTransaction(e)
	default:
		return errors.New("the entry is not valid")
	}
}

func (uc TransactionUseCase) MoveFromCashOnHoldAccountToCashAccount(tid uint) error {

	t, err := uc.TransactionRepository.FindOneByID(tid)
	if err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	t.OnHoldSettled = tools.NewBoolPointer(true)
	dr := &model.Transaction{
		AccountID:   t.AccountID,
		Description: t.Description,
		SourceID:    t.SourceID,
		BalanceType: t.BalanceType,
		Debit:       t.Credit,
	}
	cr := &model.Transaction{
		AccountID:   t.AccountID,
		Description: t.Description,
		SourceID:    t.SourceID,
		BalanceType: model.AvailableBalanceType,
		Credit:      t.Credit,
	}

	if err := uc.TransactionOperation.MoveMoneyFromCashOnHoldToCashAccount(t, dr, cr); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}
