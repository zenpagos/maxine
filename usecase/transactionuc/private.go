package transactionuc

import (
	"gitlab.com/zenpagos/maxine/model"
	"gitlab.com/zenpagos/tools"
)

func (uc TransactionUseCase) persistTransaction(t *model.Transaction) error {
	if err := t.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := uc.TransactionRepository.Insert(t); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc TransactionUseCase) creditToCashAccount(e *model.Entry) error {
	t := &model.Transaction{
		BalanceType: model.AvailableBalanceType,
		AccountID:   e.AccountID,
		SourceID:    e.SourceID,
		Description: e.Description,
		Credit:      e.Amount,
	}

	if err := uc.persistTransaction(t); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc TransactionUseCase) creditToCashOnHoldAccount(e *model.Entry) error {
	t := &model.Transaction{
		BalanceType:   model.OnHoldBalanceType,
		AccountID:     e.AccountID,
		SourceID:      e.SourceID,
		Description:   e.Description,
		Credit:        e.Amount,
		OnHoldSettled: tools.NewBoolPointer(false),
		OnHoldUntil:   tools.NextUnixTimePointer(&e.DaysOnHold),
	}

	if err := uc.persistTransaction(t); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc TransactionUseCase) creditToCashInBankAccount(e *model.Entry) error {
	t := &model.Transaction{
		BalanceType: model.CashedOutBalanceType,
		AccountID:   e.AccountID,
		SourceID:    e.SourceID,
		Description: e.Description,
		Credit:      e.Amount,
	}

	if err := uc.persistTransaction(t); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc TransactionUseCase) debitFromCashAccount(e *model.Entry) error {
	t := &model.Transaction{
		BalanceType: model.AvailableBalanceType,
		AccountID:   e.AccountID,
		SourceID:    e.SourceID,
		Description: e.Description,
		Debit:       e.Amount,
	}

	if err := uc.persistTransaction(t); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc TransactionUseCase) createDepositTransaction(e *model.Entry) error {
	// if the entry has defined days on hold it is credited in cash on hold account
	if e.DaysOnHold > 0 {
		if err := uc.creditToCashOnHoldAccount(e); err != nil {
			uc.log.WithError(err).Error()
			return err
		}
		return nil
	}

	// if the entry doesn't have defined days on hold it is credited in the cash account.
	if err := uc.creditToCashAccount(e); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc TransactionUseCase) createWithdrawalTransaction(e *model.Entry) error {
	// create a transaction in the database to debitFromCashAccount from cash account
	if err := uc.debitFromCashAccount(e); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	// create a transaction in the database to creditToCashInBankAccount to cash in bank account
	if err := uc.creditToCashInBankAccount(e); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}
