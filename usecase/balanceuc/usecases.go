// Package registration represents the concrete implementation of BalanceUseCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package balanceuc

import (
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/maxine/model"
	"gitlab.com/zenpagos/maxine/repository"
	"gitlab.com/zenpagos/maxine/usecase"
)

type BalanceUseCase struct {
	log               *logrus.Logger
	BalanceRepository repository.BalanceRepositoryInterface
}

func NewBalanceUseCase(log *logrus.Logger, br repository.BalanceRepositoryInterface) usecase.BalanceUseCaseInterface {
	return BalanceUseCase{
		log:               log,
		BalanceRepository: br,
	}
}

func (uc BalanceUseCase) ListBalances(aid uuid.UUID) ([]*model.Balance, error) {
	defaultBalances := []*model.Balance{
		{
			Type:   model.AvailableBalanceType,
			Amount: 0,
		},
		{
			Type:   model.OnHoldBalanceType,
			Amount: 0,
		},
		{
			Type:   model.InTransitBalanceType,
			Amount: 0,
		},
		{
			Type:   model.CashedOutBalanceType,
			Amount: 0,
		},
	}

	actualBalance, err := uc.BalanceRepository.FindByAccountID(aid)
	if err != nil {
		uc.log.WithError(err).Error()
		return nil, err
	}

	for _, b := range defaultBalances {
		for _, ab := range actualBalance {
			if b.Type == ab.Type {
				b.Amount = ab.Amount
			}
		}
	}

	return defaultBalances, nil
}
