// Package registration represents the concrete implementation of EntryUseCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package entryuc

import (
	"errors"
	"github.com/RichardKnop/machinery/v1"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/maxine/model"
	"gitlab.com/zenpagos/maxine/repository"
	"gitlab.com/zenpagos/maxine/usecase"
)

type EntryUseCase struct {
	log                   *logrus.Logger
	MachineryServer       *machinery.Server
	EntryRepository       repository.EntryRepositoryInterface
	TransactionRepository repository.TransactionRepositoryInterface
	BalanceRepository     repository.BalanceRepositoryInterface
}

func NewEntryUseCase(
	log *logrus.Logger,
	ms *machinery.Server,
	er repository.EntryRepositoryInterface,
	tr repository.TransactionRepositoryInterface,
	br repository.BalanceRepositoryInterface,
) usecase.EntryUseCaseInterface {
	return EntryUseCase{
		log:                   log,
		MachineryServer:       ms,
		EntryRepository:       er,
		TransactionRepository: tr,
		BalanceRepository:     br,
	}
}

func (uc EntryUseCase) CanCashOut(aid uuid.UUID, a int64) (bool, error) {
	b, err := uc.BalanceRepository.FindOneByTypeAndAccountID(model.AvailableBalanceType, aid)
	if err != nil {
		uc.log.WithError(err).Error()
		return false, err
	}

	if a > b.Amount {
		return false, nil
	}

	return true, nil
}

func (uc EntryUseCase) CreateEntry(e *model.Entry) error {
	// validate data
	if err := e.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	// validate if the account has cash
	if e.Source == model.CashOutSource {
		aid := e.AccountID
		a := e.Amount
		hasCredit, err := uc.CanCashOut(aid, a)
		if err != nil {
			uc.log.WithError(err).Error()
			return err
		}
		if hasCredit != true {
			err := errors.New("the balance is not enough")
			uc.log.WithError(err).Error()
			return err
		}
	}

	// save in database
	if err := uc.EntryRepository.Insert(e); err != nil {
		return err
	}

	return nil
}

//func (euc EntryUseCase) Deposit(e *model.Entry) error {
//	// insert the new entry to database
//	if err := euc.EntryRepository.Insert(e); err != nil {
//		return err
//	}
//
//	// create a transaction in the database to credit to cash account
//	t := model.Transaction{
//		AccountID:       e.AccountID,
//		Description: e.Description,
//		SourceID:      e.SourceID,
//		BalanceType:     model.AvailableBalanceType,
//		Credit:      e.Amount,
//	}
//
//	// select the CASH_ON_HOLD account if there DaysOnHold is greater than 0
//	if e.DaysOnHold != nil {
//		onHoldUntil := time.Now().UTC().AddDate(0, 0, int(*e.DaysOnHold))
//		onHoldUntilUnix := onHoldUntil.Unix()
//
//		t.BalanceType = model.OnHoldBalanceType
//		t.OnHoldSettled = false
//		t.OnHoldUntil = &onHoldUntilUnix
//	}
//
//	if err := t.Validate(); err != nil {
//		return err
//	}
//	if err := t.Complete(); err != nil {
//		return err
//	}
//	if err := euc.TransactionRepository.Insert(&t); err != nil {
//		return err
//	}
//
//	// send a task to move the cash on hold account to cash account
//	if t.BalanceType == model.OnHoldBalanceType && t.OnHoldSettled == false {
//		onHoldUntil := time.Now().UTC().AddDate(0, 0, int(*e.DaysOnHold))
//		signature := &tasks.Signature{
//			Name: "MoveFromCashOnHoldAccountToCashAccount",
//			ETA:  &onHoldUntil,
//			Args: []tasks.Arg{
//				{
//					Source:  "uint",
//					Value: t.ID,
//				},
//			},
//		}
//
//		if _, err := euc.MachineryServer.SendTask(signature); err != nil {
//			return err
//		}
//	}
//
//	return nil
//}
//
//// CanCashOut checks if is possible withdrawal from the CASH_ACCOUNT
//// it should have enough balance to create a withdrawal.
//func (euc EntryUseCase) CanCashOut(e *model.Entry) (bool, error) {
//	balance, err := euc.TransactionRepository.GetBalanceByTypeAndAccountID(model.AvailableBalanceType, e.AccountID)
//	if err != nil {
//		return false, err
//	}
//
//	if e.Amount > balance {
//		return false, nil
//	}
//
//	return true, nil
//}
//
//func (euc EntryUseCase) Withdrawal(e *model.Entry) error {
//	// validate if the account has credit to withdrawal
//	hasCredit, err := euc.CanCashOut(e)
//	if err != nil {
//		return err
//	}
//	if hasCredit != true {
//		return errors.New("the balance is not enough")
//	}
//
//	// insert the new withdrawal to database
//	if err := euc.EntryRepository.Insert(e); err != nil {
//		return err
//	}
//
//	// create a transaction in the database to debit from cash account
//	dr := model.Transaction{
//		AccountID:       e.AccountID,
//		Description: e.Description,
//		SourceID:      e.SourceID,
//		BalanceType:     model.AvailableBalanceType,
//		Debit:       e.Amount,
//	}
//	if err := dr.Validate(); err != nil {
//		return err
//	}
//	if err := dr.Complete(); err != nil {
//		return err
//	}
//	if err := euc.TransactionRepository.Insert(&dr); err != nil {
//		return err
//	}
//
//	// create a transaction in the database to credit to cash in bank account
//	cr := model.Transaction{
//		AccountID:       e.AccountID,
//		Description: e.Description,
//		SourceID:      e.SourceID,
//		BalanceType:     model.CashedOutBalanceType,
//		Credit:      e.Amount,
//	}
//	if err := cr.Validate(); err != nil {
//		return err
//	}
//	if err := cr.Complete(); err != nil {
//		return err
//	}
//	if err := euc.TransactionRepository.Insert(&cr); err != nil {
//		return err
//	}
//
//	return nil
//}
//
//// CanTransfer checks if is possible transfer from the CASH_ACCOUNT
//// it should have enough balance to create a withdrawal.
//func (euc EntryUseCase) CanTransfer(e *model.Entry) (bool, error) {
//	balance, err := euc.TransactionRepository.GetBalanceByTypeAndAccountID(model.AvailableBalanceType, e.AccountID)
//	if err != nil {
//		return false, err
//	}
//
//	if e.Amount > balance {
//		return false, nil
//	}
//
//	return true, nil
//}
//
//// Transfer money from an user to another.
//// In order to achieve that, this use case perform the following tasks:
////
//// - Validate the transfer model.
//// - Validate if there is enough money in the user balance.
//// - Insert a new transfer record in the database for the owner.
//// - Insert a new transaction record in the database to debit money from the owner CASH account.
//// - Insert a new transaction record in the database to credit money to the receiver in his CASH account.
//// - And finally, insert a new deposit record in the database for the receiver.
//func (euc EntryUseCase) Transfer(e *model.Entry) error {
//	// validate if the account balance
//	hasCredit, err := euc.CanTransfer(e)
//	if err != nil {
//		return err
//	}
//	if hasCredit != true {
//		return errors.New("the balance is not enough")
//	}
//
//	// create the new transfer record in the database for the owner
//	if err := euc.EntryRepository.Insert(e); err != nil {
//		return err
//	}
//
//	// create a transaction record in the database to debit money from owner
//	dr := model.Transaction{
//		AccountID:       e.AccountID,
//		Description: e.Description,
//		SourceID:      e.SourceID,
//		BalanceType:     model.AvailableBalanceType,
//		Debit:       e.Amount,
//		ReceiverID:  *e.ReceiverID,
//	}
//	if err := dr.Validate(); err != nil {
//		return err
//	}
//	if err := dr.Complete(); err != nil {
//		return err
//	}
//	if err := euc.TransactionRepository.Insert(&dr); err != nil {
//		return err
//	}
//
//	// create a transaction record in the database to credit money to receiver
//	cr := model.Transaction{
//		AccountID:       *e.ReceiverID,
//		Description: e.Description,
//		SourceID:      e.SourceID,
//		BalanceType:     model.AvailableBalanceType,
//		Credit:      e.Amount,
//		ReceiverID:  *e.ReceiverID,
//	}
//	if err := cr.Validate(); err != nil {
//		return err
//	}
//	if err := cr.Complete(); err != nil {
//		return err
//	}
//	if err := euc.TransactionRepository.Insert(&cr); err != nil {
//		return err
//	}
//
//	// create a new deposit record in the database for the receiver
//	d := model.Entry{
//		Source:        model.PaymentSource,
//		AccountID:       *e.ReceiverID,
//		Description: e.Description,
//		SourceID:      e.SourceID,
//		Amount:      e.Amount,
//	}
//	if err := d.Validate(); err != nil {
//		return err
//	}
//	if err := euc.EntryRepository.Insert(&d); err != nil {
//		return err
//	}
//
//	return nil
//}
