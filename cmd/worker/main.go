package main

import (
	"github.com/RichardKnop/machinery/v1"
	machineryconf "github.com/RichardKnop/machinery/v1/config"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/maxine/config"
	"gitlab.com/zenpagos/maxine/container/worker_container"
	"gitlab.com/zenpagos/maxine/repository/mysqlrepo"
)

var log = logrus.New()

func init() {
	log.SetReportCaller(true)
	log.SetLevel(logrus.InfoLevel)
	log.SetFormatter(&logrus.JSONFormatter{
		PrettyPrint: true,
	})
}

func machineryInit() (*machinery.Server, error) {
	var cnf = machineryconf.Config{
		Broker:        "amqp://guest:guest@rabbitmq:5672",
		DefaultQueue:  "queue",
		ResultBackend: "amqp://guest:guest@rabbitmq:5672",
		AMQP: &machineryconf.AMQPConfig{
			Exchange:     "machinery_exchange",
			ExchangeType: "direct",
			BindingKey:   "machinery_task",
		},
	}

	return machinery.NewServer(&cnf)
}

func main() {
	// read configuration
	conf, err := config.ReadConfig(log)
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf, log)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}

	ms, err := machineryInit()
	if err != nil {
		log.Panicf("can not create Machinery server!: %v", err)
	}

	c := worker_container.InitializeContainer(db)

	if err := ms.RegisterTask("MoveFromCashOnHoldAccountToCashAccount",
		c.TransactionUseCase.MoveFromCashOnHoldAccountToCashAccount); err != nil {
		log.Panicf("could not register the task: %v", err)
	}

	worker := ms.NewWorker("worker-1", 10)

	err = worker.Launch()
	if err != nil {
		log.Panicf("could not launch worker: %v", err)
	}
}
