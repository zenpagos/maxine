package main

import (
	"fmt"
	"github.com/RichardKnop/machinery/v1"
	machineryconf "github.com/RichardKnop/machinery/v1/config"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/maxine/config"
	"gitlab.com/zenpagos/maxine/container"
	"gitlab.com/zenpagos/maxine/interface/igrpc"
	pbv1 "gitlab.com/zenpagos/maxine/proto/v1"
	"gitlab.com/zenpagos/maxine/repository/mysqlrepo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"net"
)

var log = logrus.New()

func init() {
	log.SetReportCaller(true)
	log.SetLevel(logrus.InfoLevel)
	log.SetFormatter(&logrus.JSONFormatter{
		PrettyPrint: true,
	})
}

func machineryInit() (*machinery.Server, error) {
	var cnf = machineryconf.Config{
		Broker:        "amqp://guest:guest@rabbitmq:5672",
		DefaultQueue:  "queue",
		ResultBackend: "amqp://guest:guest@rabbitmq:5672",
		AMQP: &machineryconf.AMQPConfig{
			Exchange:     "machinery_exchange",
			ExchangeType: "direct",
			BindingKey:   "machinery_task",
		},
	}

	return machinery.NewServer(&cnf)
}

func main() {
	// read configuration
	conf, err := config.ReadConfig(log)
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf, log)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}

	ms, err := machineryInit()
	if err != nil {
		log.Panicf("failed to create Machinery server!: %v", err)
	}

	addr := fmt.Sprintf(":%d", conf.Service.Port)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Panicf("failed to listen: %v", err)
	}

	serviceContainer := container.InitializeContainer(db, log, ms)

	gRPCServer := grpc.NewServer()

	maxineService := igrpc.NewGRPCServer(log, serviceContainer)
	pbv1.RegisterMaxineServiceServer(gRPCServer, maxineService)

	healthService := igrpc.NewHealthChecker()
	grpc_health_v1.RegisterHealthServer(gRPCServer, healthService)

	if err := gRPCServer.Serve(lis); err != nil {
		log.Panicf("failed to serve: %v", err)
	}
}
