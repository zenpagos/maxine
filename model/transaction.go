// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	uuid "github.com/satori/go.uuid"
)

type Transaction struct {
	ID            uuid.UUID `sql:"type:varchar(255);primary_key;"`
	CreatedAt     int64     `sql:"type:int;not null"`
	AccountID     uuid.UUID `sql:"type:varchar(255);not null"`
	BalanceType   string    `sql:"type:varchar(255);not null"`
	SourceID      uuid.UUID `sql:"type:varchar(255);not null"`
	Description   string    `sql:"type:varchar(255);not null"`
	Debit         int64     `sql:"type:int(25)"`
	Credit        int64     `sql:"type:int(25)"`
	OnHoldSettled *bool     `sql:"type:boolean"`
	OnHoldUntil   *int64    `sql:"type:int"`
}

func (t Transaction) Validate() error {
	return validation.ValidateStruct(&t,
		validation.Field(&t.AccountID, validation.Required, is.UUIDv4),
		validation.Field(&t.BalanceType, balanceTypeRule...),
		validation.Field(&t.SourceID, validation.Required, is.UUID),
		validation.Field(&t.Description, validation.Required, validation.Length(0, 255)),
	)
}
