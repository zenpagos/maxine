// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	uuid "github.com/satori/go.uuid"
)

type Entry struct {
	ID          uuid.UUID `sql:"type:varchar(255);primary_key;"`
	CreatedAt   int64     `sql:"type:int;not null"`
	AccountID   uuid.UUID `sql:"type:varchar(255);not null"`
	Source      string    `sql:"type:varchar(20);not null"`
	SourceID    uuid.UUID `sql:"type:varchar(255);not null"`
	Description string    `sql:"type:varchar(255);not null"`
	Amount      int64     `sql:"type:int(25);not null"`
	DaysOnHold  int64     `sql:"type:int;not null"`
}

func (e Entry) Validate() error {
	return validation.ValidateStruct(&e,
		validation.Field(&e.AccountID, validation.Required, is.UUIDv4),
		validation.Field(&e.Source, sourceRule...),
		validation.Field(&e.SourceID, validation.Required, is.UUIDv4),
		validation.Field(&e.Description, validation.Required, validation.Length(0, 255)),
		validation.Field(&e.Amount, validation.Required),
	)
}
