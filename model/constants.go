// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

const (
	PaymentSource = "payment"
	CashOutSource = "cash.out"

	// this is money that is able to be cash-out o payout
	AvailableBalanceType = "available"
	// this is money that is waiting to be received in our bank accounts
	OnHoldBalanceType = "on.hold"
	// this is money that will be transfer to the bank account of an account
	InTransitBalanceType = "in.transit"
	// this is money that was transferred to the bank account of an account
	CashedOutBalanceType = "cashed.out"
)
