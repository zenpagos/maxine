#!/bin/bash

SVC_DEPLOYMENT=maxine

docker build -f docker/Dockerfile -t 127.0.0.1:5000/maxine .

docker push 127.0.0.1:5000/maxine

helm upgrade --install \
  -f scripts/maxine-values.yaml \
  ${SVC_DEPLOYMENT} kubernetes/maxine
