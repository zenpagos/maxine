package igrpc

import (
	"context"
	_ "github.com/jnewmano/grpc-json-proxy/codec"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/maxine/container"
	"gitlab.com/zenpagos/maxine/model"
	pbv1 "gitlab.com/zenpagos/maxine/proto/v1"
)

type gRPCServer struct {
	log       *logrus.Logger
	container container.Container
}

func NewGRPCServer(log *logrus.Logger, c container.Container) pbv1.MaxineServiceServer {
	return &gRPCServer{
		log:       log,
		container: c,
	}
}

func (s *gRPCServer) CreateEntry(
	c context.Context,
	req *pbv1.EntryRequest,
) (*pbv1.EntryResponse, error) {

	e := &model.Entry{
		AccountID:   uuid.FromStringOrNil(req.AccountID),
		Source:      req.Source,
		SourceID:    uuid.FromStringOrNil(req.SourceID),
		Description: req.Description,
		Amount:      req.Amount,
		DaysOnHold:  req.DaysOnHold,
	}

	// create a new entry
	if err := s.container.EntryUseCase.CreateEntry(e); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	// create transaction(s) for the new entry
	if err := s.container.TransactionUseCase.CreateTransactionFromEntry(e); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	res := &pbv1.EntryResponse{
		ID:          e.ID.String(),
		CreatedAt:   e.CreatedAt,
		AccountID:   e.AccountID.String(),
		Source:      e.Source,
		SourceID:    e.SourceID.String(),
		Description: e.Description,
		Amount:      e.Amount,
		DaysOnHold:  e.DaysOnHold,
	}

	return res, nil
}

func (s *gRPCServer) CanCashOut(
	c context.Context,
	req *pbv1.CanCashOutRequest,
) (*pbv1.CanCashOutResponse, error) {

	amount := req.Amount
	accountID := uuid.FromStringOrNil(req.AccountID)

	r, err := s.container.EntryUseCase.CanCashOut(accountID, amount)
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	res := &pbv1.CanCashOutResponse{
		AccountID: accountID.String(),
		Amount:    amount,
		Result:    r,
	}

	return res, nil
}

func (s *gRPCServer) ListBalances(
	c context.Context,
	req *pbv1.ListBalancesRequest,
) (*pbv1.ListBalancesResponse, error) {

	accountID := uuid.FromStringOrNil(req.AccountID)

	bs, err := s.container.BalanceUseCase.ListBalances(accountID)
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	var balances []*pbv1.Balance
	for _, b := range bs {
		balances = append(balances, &pbv1.Balance{
			Type:   b.Type,
			Amount: b.Amount,
		})
	}

	res := &pbv1.ListBalancesResponse{
		Balances: balances,
	}

	return res, nil
}
