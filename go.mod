module gitlab.com/zenpagos/maxine

go 1.14

require (
	github.com/RichardKnop/machinery v1.7.4
	github.com/go-ozzo/ozzo-validation/v4 v4.1.0
	github.com/golang/protobuf v1.3.5
	github.com/google/wire v0.4.0
	github.com/jinzhu/gorm v1.9.12
	github.com/jnewmano/grpc-json-proxy v0.0.0-20200227201450-d2f9a1e2ec3d
	github.com/onsi/ginkgo v1.10.1 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/viper v1.6.3
	github.com/stretchr/testify v1.5.1 // indirect
	gitlab.com/zenpagos/tools v0.0.0-20200616225453-f02e1e8b6545
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200409092240-59c9f1ba88fa // indirect
	google.golang.org/genproto v0.0.0-20200409111301-baae70f3302d // indirect
	google.golang.org/grpc v1.28.1
)
