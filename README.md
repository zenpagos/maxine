# Maxine
Maxine was thought as a micro-service that will store all the transactional operations of 
a user or any entity that need to store monetary operations.

## Capabilities
* Deposit money to an user account.
* Withdrawal money from an user account.
* Transfer money from an user account to another.
* Chargeback money from an user account.
* Allow attach fee definitions in each operation.
* Allow configure a deposit to put the money on hold for a period of time and automatically be moved to a available state.
* Allow create a deposit and split it in several accounts and also configure a fee for each receiver.

##  Account Schemas
Maxine handle the transactions between four virtual accounts. 
Yes, we said virtual because Maxine don't handle real money, it just creates records in a database:

* **CASH**: This account contains all the money received for a user via Deposit o Transfer. 
    The money in this account is the only that could be used to Withdrawal to a bank account or Transfer to another user.  
* **CASH_IN_BANK**: This account contains all the money Withdrawal to a bank account.
* **CASH_ON_HOLD**: This account contains all the money received via Deposit that is necessary put in hold for a period of time, 
    after this Maxine moves the money to the CASH account.
* **CHARGEBACK**: This account contains all the money that was reverted via a Chargeback. 
    Maxine moves money from the CASH account to this one when the CASH account has enough money. 
    
## Movements
In Maxine you can create four kind of movements:
* **Deposit**: This operation increment the money in the CASH account or in the CASH_ON_HOLD 
    account (if the deposit was created to put the money in hold for a while).
* **Withdrawal**: This operation emulates when a user withdrawal money to his real bank account, 
    so Maxine decrements the money in the CASH account and increments the CASH_IN_BANK account with the same amount, 
    at least that a fee where defined at the moment of creating a Withdrawal.
* **Transfer**: This operation move money from an user to another, in which case Maxine decrement the CASH account of an
    user and increment the CASH account of another user, also create a Deposit movement to illustrate the new income in the
    CASH account.
* **Chargeback**: TBD