//+build wireinject

package worker_container

import (
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/maxine/repository/mysqlrepo"
	"gitlab.com/zenpagos/maxine/usecase/transactionuc"
)

func InitializeContainer(db *gorm.DB) Container {
	wire.Build(
		// Repositories
		mysqlrepo.NewTransactionRepository,
		mysqlrepo.NewTransactionOperation,
		// Use Cases
		transactionuc.NewTransactionUseCase,
		NewContainer)

	return Container{}
}
