package worker_container

import "gitlab.com/zenpagos/maxine/usecase"

type Container struct {
	TransactionUseCase usecase.TransactionUseCaseInterface
}

func NewContainer(tuc usecase.TransactionUseCaseInterface) Container {
	return Container{
		TransactionUseCase: tuc,
	}
}
