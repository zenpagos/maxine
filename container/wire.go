//+build wireinject

package container

import (
	"github.com/RichardKnop/machinery/v1"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/maxine/repository/mysqlrepo"
	"gitlab.com/zenpagos/maxine/usecase/balanceuc"
	"gitlab.com/zenpagos/maxine/usecase/entryuc"
	"gitlab.com/zenpagos/maxine/usecase/transactionuc"
)

func InitializeContainer(db *gorm.DB, log *logrus.Logger, ms *machinery.Server) Container {
	wire.Build(
		// Repositories
		mysqlrepo.NewEntryRepository,
		mysqlrepo.NewTransactionRepository,
		mysqlrepo.NewTransactionOperation,
		mysqlrepo.NewBalanceRepository,
		// Use Cases
		entryuc.NewEntryUseCase,
		transactionuc.NewTransactionUseCase,
		balanceuc.NewBalanceUseCase,
		NewContainer,
	)

	return Container{}
}
