package container

import "gitlab.com/zenpagos/maxine/usecase"

type Container struct {
	EntryUseCase       usecase.EntryUseCaseInterface
	TransactionUseCase usecase.TransactionUseCaseInterface
	BalanceUseCase     usecase.BalanceUseCaseInterface
}

func NewContainer(
	euc usecase.EntryUseCaseInterface,
	tuc usecase.TransactionUseCaseInterface,
	buc usecase.BalanceUseCaseInterface,
) Container {
	return Container{
		EntryUseCase:       euc,
		TransactionUseCase: tuc,
		BalanceUseCase:     buc,
	}
}
