// Package repository and it's sub-package represents data persistence service, mainly access database,
// but also including data persisted by other Micro-service.
// For Micro-server, only the interface is defined in this package, the data transformation code is in adapter package
// This is the top level package and it only defines interface, and all implementations are defined in sub-package
// Use case package depends on it.
package repository

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/maxine/model"
)

type EntryRepositoryInterface interface {
	Insert(d *model.Entry) error
}

type TransactionRepositoryInterface interface {
	Insert(t *model.Transaction) error
	FindOneByID(tid uint) (*model.Transaction, error)
}

type TransactionOperationInterface interface {
	MoveMoneyFromCashOnHoldToCashAccount(t *model.Transaction, dr *model.Transaction, cr *model.Transaction) error
}

type BalanceRepositoryInterface interface {
	FindOneByTypeAndAccountID(balanceType string, accountID uuid.UUID) (*model.Balance, error)
	FindByAccountID(accountID uuid.UUID) ([]model.Balance, error)
}
