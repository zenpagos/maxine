package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/maxine/model"
	"gitlab.com/zenpagos/maxine/repository"
)

type TransactionRepository struct {
	DB *gorm.DB
}

func NewTransactionRepository(db *gorm.DB) repository.TransactionRepositoryInterface {
	return TransactionRepository{
		DB: db,
	}
}

func (tr TransactionRepository) Insert(t *model.Transaction) error {
	return tr.DB.Create(&t).Error
}

func (tr TransactionRepository) FindOneByID(tid uint) (*model.Transaction, error) {
	var t model.Transaction
	if err := tr.DB.First(&t, tid).Error; err != nil {
		return nil, err
	}

	return &t, nil
}
