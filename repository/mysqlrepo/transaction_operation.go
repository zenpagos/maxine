package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/maxine/model"
	"gitlab.com/zenpagos/maxine/repository"
)

type TransactionOperation struct {
	DB *gorm.DB
}

func NewTransactionOperation(db *gorm.DB) repository.TransactionOperationInterface {
	return TransactionOperation{
		DB: db,
	}
}

func (to TransactionOperation) MoveMoneyFromCashOnHoldToCashAccount(t *model.Transaction,
	dr *model.Transaction, cr *model.Transaction) error {
	// execute inside a transaction block
	return to.DB.Transaction(func(tx *gorm.DB) error {
		if err := tx.Save(t).Error; err != nil {
			return err
		}
		if err := tx.Create(dr).Error; err != nil {
			return err
		}
		if err := tx.Create(cr).Error; err != nil {
			return err
		}

		return nil
	})
}
