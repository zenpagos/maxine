package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/maxine/model"
	"gitlab.com/zenpagos/maxine/repository"
)

type BalanceRepository struct {
	DB *gorm.DB
}

func NewBalanceRepository(db *gorm.DB) repository.BalanceRepositoryInterface {
	return BalanceRepository{
		DB: db,
	}
}

func (br BalanceRepository) FindOneByTypeAndAccountID(bt string, aid uuid.UUID) (*model.Balance, error) {
	balance := &model.Balance{}
	if err := br.DB.Model(model.Transaction{}).
		Select("balance_type AS type, (SUM(credit) - SUM(debit)) AS amount").
		Where("balance_type = ?", bt).
		Where("account_id = ?", aid.String()).
		Group("balance_type").
		Scan(balance).Error; err != nil {
		// when there are not transactions for a balance type
		// the database return a NotFound error, so in this case
		// we will return an empty balance with amount 0
		if err == gorm.ErrRecordNotFound {
			return balance, nil
		}

		return nil, err
	}

	return balance, nil
}

func (br BalanceRepository) FindByAccountID(aid uuid.UUID) ([]model.Balance, error) {
	var balances []model.Balance
	if err := br.DB.Model(model.Transaction{}).
		Select("balance_type AS type, (SUM(credit) - SUM(debit)) AS amount").
		Where("account_id = ?", aid.String()).
		Group("balance_type").
		Scan(&balances).Error; err != nil {
		return nil, err
	}

	return balances, nil
}
