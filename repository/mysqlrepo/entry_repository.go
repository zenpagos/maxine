package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/maxine/model"
	"gitlab.com/zenpagos/maxine/repository"
)

type EntryRepository struct {
	DB *gorm.DB
}

func NewEntryRepository(db *gorm.DB) repository.EntryRepositoryInterface {
	return EntryRepository{
		DB: db,
	}
}

func (er EntryRepository) Insert(e *model.Entry) error {
	return er.DB.Create(&e).Error
}
